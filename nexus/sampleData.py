####  getSchedule Sample Data ####
# contents = []
# contents.append(["startTime", "2020-06-09T00:00:00", {}])
# contents.append(["endTime", "2020-06-09T23:00:00", {}])
# contents.append(["properties", "", {}])
# contents.append(["fields", "", {}])
#
# properties = []
# properties.append(["property", "True", {"name": "songList"}])
#
# fields = []
# fields.append(["field", "", {"id": "12"}])
# fields.append(["field", "", {"name": "Artist"}])
# fields.append(["field", "", {"name": "Title"}])
#
# data = []
# data.append(contents)
# data.append(properties)
# data.append(fields)

#### getStationProperty Sample Data ####
# contents = []
# contents.append(["properties", "", {}])
#
# properties = []
# properties.append(["property", "", {"name": "testing"}])
#
# data = []
# data.append(contents)
# data.append(properties)

#### setStationProperty Sample Data ####
# contents = []
# contents.append(["properties", "", {}])
#
# properties = []
# properties.append(["property", "123", {"name": "testing"}])
#
# data = []
# data.append(contents)
# data.append(properties)

#### getSongsByList Sample Data ####
# contents = []
# contents.append(["songList", "", {"id": "1"}])
# contents.append(["fields", "", {}])
#
# fields = []
# fields.append(["fields", "", {"id": "12"}])
# fields.append(["fields", "", {"name": "Artist"}])
# fields.append(["fields", "", {"name": "Title"}])
#
# data = []
# data.append(contents)
# data.append(fields)

#### getSongByQuery Sample Data ####
# contents = []
# contents.append(["query", "", {}])
# contents.append(["fields", "", {}])
#
# query = []
# query.append(["categories", "", {}])
# query.append(["filters", "", {}])
# query.append(["sortKeys", "", {}])
#
# categories = []
# categories.append(["category", "", {"code": "NEW"}])
#
# filters = []
# filters.append(["filter", "", {"type": "fieldName", "target": "Title", "operator": "contains", "value": "love"}])
#
# sortKeys = []
# sortKeys.append(["sortKey", "", {"type": "fieldName", "target": "Title", "order": "asc"}])
#
# fields = []
# fields.append(["fields", "", {"id": "12"}])
# fields.append(["fields", "", {"name": "Artist"}])
# fields.append(["fields", "", {"name": "Title"}])
#
# data = []
# data.append(contents)
# data.append(query)
# data.append(categories)
# data.append(filters)
# data.append(sortKeys)
# data.append(fields)

#### getSongInfo Sample Data ####
# contents = []
# contents.append(["songList", "", {}])
# contents.append(["fields", "", {}])
#
# songList = []
# songList.append(["song", "", {"songId": "146962"}])
#
# fields = []
# fields.append(["field", "", {"id": "101"}])
# fields.append(["field", "", {"name": "Artist"}])
# fields.append(["field", "", {"name": "Title"}])
#
# data = []
# data.append(contents)
# data.append(songList)
# data.append(fields)

#### getHistory Sample Data ####
# contents = []
# contents.append(["song", "", {"songId": "146962"}])
# contents.append(["maxHistories", "10", {}])
#
# data = []
# data.append(contents)

#### getSongHistory Sample Data ####
# contents = []
# contents.append(["maxHistories", "10", {}])
# contents.append(["archiveHistory", "1", {}])
# contents.append(["songList", "", {}])
#
# songList = []
# songList.append(["song", "", {"songId": "163109"}])
#
# data = []
# data.append(contents)
# data.append(songList)

#### getScheduleHistory ####
# contents = []
# contents.append(["startTime", "2020-06-09T00:00:00", {}])
# contents.append(["endTime", "2020-06-09T23:00:00", {}])
# contents.append(["query", "", {}])
#
# query = []
# query.append(["categories", "", {}])
# query.append(["filters", "", {}])
# query.append(["sortKeys", "", {}])
# query.append(["properties", "", {}])
#
# categories = []
# categories.append(["category", "", {"code": "PPR"}])
#
# filters = []
# filters.append(["filter", "", {"type": "fieldName", "target": "Title", "operator": "contains", "value": "love"}])
#
# sortKeys = []
# sortKeys.append(["sortKey", "", {"type": "fieldName", "target": "Title", "order": "asc"}])
#
# properties = []
# properties.append(["property", "25", {"name": "maxRecs"}])
#
# data = []
# data.append(contents)
# data.append(query)
# data.append(categories)
# data.append(filters)
# data.append(sortKeys)
# data.append(properties)

# def printXML(xmlRoot):
#    dest = xml.dom.minidom.parseString(ET.tostring(xmlRoot, encoding='utf8').decode('utf8'))
#    xml_pretty_str = dest.toprettyxml()
#    print(xml_pretty_str)
#    # Code for printing to a file
#    sample = open('samplefile.txt', 'w')
#    print(xml_pretty_str, file=sample)  #.encode('utf-8')
#    sample.close()
