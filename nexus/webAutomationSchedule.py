from nexus.api import nexusAPI
import datetime

def getAutomationSchedule(startDate=datetime.date.today(), endDate=datetime.date.today()):
    one_day = datetime.timedelta(days=1)
    airSchedule = {}  # songID and airtime hour
    categories = {}   # store musicmaster categories
    songAnalysis = {} # analyze song data
    webSchedule = {}  # store majority played genres (max 2) for each hour requested

    #########################  START Build XML Data Request  #########################
    contents = []
    contents.append(["startTime", str(startDate) + "T00:00:00", {}])
    contents.append(["endTime", str(startDate) + "T23:00:00", {}])

    contents.append(["properties", "", {}])
    contents.append(["fields", "", {}])

    properties = []
    properties.append(["property", "True", {"name": "songList"}])

    fields = []
    fields.append(["field", "", {"name": "Category"}])
    fields.append(["field", "", {"name": "Non-Music"}])

    data = []
    data.append(contents)
    data.append(properties)
    data.append(fields)
    #########################  END Build XML Data Request  #########################

    request = nexusAPI.NexusAPI()

    # retrieve categories that exist in MusicMaster
    root = request.getCategoryList()
    for category in root[0][0].findall('category'):
        # remove the word library or rotation
        removeWords = ['library', 'rotation']
        nameFilter = [word for word in category.get('name').split() if word.lower() not in removeWords]
        nameFilter = ' '.join(nameFilter)
        # strip whitespace and store for later comparison
        categories[category.get('code')] = nameFilter.strip()
        songAnalysis[category.get('code')] = 0

    # request and return genres for each hour
    while(startDate != endDate):
        # reset variables
        songData = {}
        songAnalysis.update((cat, 0) for cat, x in songAnalysis.items())

        # get airtime statistics and store song data
        root = request.getSchedule(data)

        # all scheduled songs
        for song in root[0][1].findall('song'):
            songID = song.get('songId')
            cat = song[0].text; nonMusic = song[1].text
            # filter out those with categories that don't exist
            if (nonMusic == "0" and cat in categories):
                # increment number per category
                songData[songID] = cat

        # airtime schedule data
        for schedule in root[0][0].findall('element'):
            songID = schedule.get('songId')
            if songID in songData.keys():
                cat = songData[songID]
                airSchedule[str(schedule.get('airDate')) + "@" + str(schedule.get('airTime'))] = cat

        # loop through 24 hours of data
        for i in range(24):
            songAnalysis.update((cat, 0) for cat, x in songAnalysis.items()) # reset category storage
            numSongs = 0 # count songs for each hour

            for key in airSchedule:
                songGenre = airSchedule[key]
                songTime = key.split("@")
                songTime[1] = int((songTime[1])[:2])

                # isolate songs for a specified day and hour
                if (songTime[1] == i and str(startDate) == songTime[0]):
                    songAnalysis[songGenre] = songAnalysis[songGenre] + 1
                    numSongs += 1

            # convert counts to percentages
            for cat in songAnalysis.keys():
                if (songAnalysis[cat] != 0):
                     songAnalysis[cat] = round(songAnalysis[cat] / numSongs * 100)

            # add functionality to test for mixture of songs.  Requirements below
            # 1. no song must play more than 40% of the time

            # get most played genre
            genre = []
            genre.append(max(songAnalysis.keys(), key=(lambda k: songAnalysis[k])))
            # get second most played genre
            songAnalysis[genre[0]] = 0
            genre.append(max(songAnalysis.keys(), key=(lambda k: songAnalysis[k])))

            # remove if it not more than 20% of the listed songs
            if (songAnalysis[genre[1]] < 20):
                genre.pop(1)

            # replace abbreviation with full category name
            genre[0] = categories[genre[0]]
            # replace second genre only if it exists.
            if (len(genre) > 1):
                genre[1] = categories[genre[1]]

            # save genres for each hour
            webSchedule[str(startDate) + "T" + str(i) + ":00:00-05:00"] = genre

        # increment day
        startDate += one_day
        data[0][0] = "startTime", str(startDate) + "T00:00:00", {}
        data[0][1] = "endTime", str(startDate) + "T23:00:00", {}
    # debug schedule
    #print(webSchedule)

    # return dictionary in the form of {key = date/hour, value = genre list of max size 2}
    return webSchedule

def getWeek(startDate=datetime.date.today()):
    # request weeks worth of automation schedule from musicmaster
    one_day = datetime.timedelta(days=1)
    schedule = getAutomationSchedule(startDate, startDate + (7 * one_day))
    #print(schedule)
    return schedule

if __name__ == '__main__':
    getWeek()