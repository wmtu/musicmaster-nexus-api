from nexus.api import nexusAPI
import datetime
import json
import os

def exportLibrarytoJSON():
    # limits applied to musicmaster query
    firstRecord = 1
    maxRecords = 5000
    # MusicMaster List of Categories
    webCategories = {}

    #########################  START Build XML Data Request  #########################
    contents = []
    contents.append(["query", "", {}])
    contents.append(["fields", "", {}])

    query = []
    query.append(["categories", "", {}])
    query.append(["filters", "", {}])
    query.append(["sortKeys", "", {}])
    query.append(["properties", "", {}])

    categories = []
    categories.append(["category", "", {"code": "NEW"}])

    filters = []
    # filters.append(["filter", "", {"type": "fieldName", "target": "Title", "operator": "contains", "value": "love"}])

    sortKeys = []
    sortKeys.append(["sortKey", "", {"type": "fieldName", "target": "Title", "order": "asc"}])

    properties = []
    properties.append(["property", str(firstRecord), {"name": "firstRec"}])
    properties.append(["property", str(maxRecords), {"name": "maxRecs"}])

    fields = []
    fields.append(["field", "", {"id": "165"}]) # Do Not Play
    fields.append(["field", "", {"id": "102"}])  # WO Category
    fields.append(["field", "", {"id": "108"}])  # Title
    fields.append(["field", "", {"id": "107"}])  # Artist
    fields.append(["field", "", {"id": "112"}])  # Album
    fields.append(["field", "", {"id": "12"}])  # Run Time
    fields.append(["field", "", {"id": "3"}])  # Category

    data = []
    data.append(contents)
    data.append(query)
    data.append(categories)
    data.append(filters)
    data.append(sortKeys)
    data.append(properties)
    data.append(fields)
    #########################  END Build XML Data Request  #########################

    request = nexusAPI.NexusAPI()

    # retrieve categories that exist in MusicMaster
    root = request.getCategoryList()
    for category in root[0][0].findall('category'):
        # remove the word library or rotation
        removeWords = ['library', 'rotation']
        nameFilter = [word for word in category.get('name').split() if word.lower() not in removeWords]
        nameFilter = ' '.join(nameFilter)
        # strip whitespace and store for later comparison
        webCategories[category.get('code')] = nameFilter.strip()

    # retrieve first interval of songs
    root = request.getSongByQuery(data)
    # get total number of songs
    numSongs = int(root[0][0].attrib['recordCount'])

    libraryData = [] # temp storage of exported song data
    while(firstRecord < numSongs):
        # request in batches
        # Do not go above 5,000 requests at a time.
        # Doing so may cause MusicMaster Out of Memory and Out of String Space Error's
        root = request.getSongByQuery(data)
        for song in root[0][0].findall('song'):
            songInfo = {}
            songInfo['Library_ID'] = str(song.attrib['songId'])
            songInfo['Song_ID'] = str(song[1].text) + "-" + str(song.attrib['cutId'])
            songInfo['DNP'] = bool(int(song[0].text))
            songInfo['Title'] = str(song[2].text)
            songInfo['Artist'] = str(song[3].text)
            songInfo['Album'] = str(song[4].text)
            songInfo['Run_Time'] = str(song[5].text)
            songInfo['Category'] = str(webCategories[song[6].text])
            songInfo['Listen'] = str("https://www.youtube.com/results?search_query=" + songInfo['Title'] + " " + songInfo['Artist']).replace(" ", '%20')
            libraryData.append(songInfo)

        firstRecord += maxRecords # increment for next request
        # update query for next request
        data[5][0] = "property", str(firstRecord), {"name": "firstRec"}
        data[5][1] = "property", str(maxRecords), {"name": "maxRecs"}

    # generate system settings
    sysConfig = {}
    webCategories = list(set(webCategories.values())) # remove dict keys and duplicates
    sysConfig['Categories'] = webCategories # category list
    sysConfig['numSongs'] = numSongs
    sysConfig['exportDate'] = str(datetime.date.today())
    sysConfig['exportTime'] = str(datetime.time())

    # create JSON file
    dataJSON = {'Configuration': sysConfig, 'SongData': libraryData}
    dir_path = os.path.dirname(os.path.realpath(__file__))
    out_file = open(dir_path + '\..\exports\webLibExport.json', "w+")
    json.dump(dataJSON, out_file, indent=6)
    out_file.close()

if __name__ == '__main__':
    exportLibrarytoJSON()