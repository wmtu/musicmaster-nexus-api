import requests
import xml.etree.ElementTree as ET

def rootAttrib(self, command, userData=""):
    self.xmlRootAttrib["command"] = command
    if (self.station != ""):
        self.xmlRootAttrib["station"] = self.station
    self.xmlRootAttrib["version"] = self.version
    self.xmlRootAttrib["client"] = self.client
    if (userData != ""):
        self.xmlRootAttrib["userData"] = userData
    return

def constructRoot(self, cmd, userData=""):
    rootAttrib(self, cmd, userData)
    root = ET.Element('mmRequest', attrib=self.xmlRootAttrib)
    ET.SubElement(root, 'contents')
    return root

class MMRequest:
    def __init__(self, client, server, port, version="1"):
        self.station = ""
        self.version = version
        self.client = client
        self.server = server
        self.port = port
        self.xmlRootAttrib = {}

    def buildRoot(self, cmd, userData=""):
        return constructRoot(self, cmd, userData)

    def getStatus(self):
        return self.sendRequest(constructRoot(self, "ping"))

    def getStations(self):
        return self.sendRequest(constructRoot(self, "getStations"))

    def runCommand(self, cmd):
        return self.sendRequest(constructRoot(self, cmd))

    def setStation(self, station):
        self.station = station

    def setVersion(self, version):
        self.version = version

    def sendRequest(self, data):
        return requests.post(self.server + ":" + self.port, ET.tostring(data, encoding='utf8').decode('utf8'))

    def addSubElement(self, parent, element="", text="", attrib={}):
        # assumes a musicmaster xml standard format
        elem = ET.SubElement(parent, element, attrib=attrib)
        if (text != ""):
            elem.text = text
        return parent