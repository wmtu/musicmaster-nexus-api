from . import mmRequest
import xml.dom.minidom
import xml.etree.ElementTree as ET

class NexusAPI:
    def __init__(self):
        self.server = ""
        self.port = ""
        self.musicMaster = ""
        self.setupBypass = 0

    #############################################
    #### MUSICMASTER REQUEST CORE OPERATIONS ####
    #############################################

    def printXML(self, xmlRoot):
        dest = xml.dom.minidom.parseString(ET.tostring(xmlRoot, encoding='utf8').decode('utf8'))
        xml_pretty_str = dest.toprettyxml()
        print(xml_pretty_str)

    # default connection setup
    # def setup1():
    #     global setupBypass, mmServer, mmPort, musicMaster
    #     if (setupBypass == 1):
    #         return
    #     mmServer = "http://10.10.50.18"
    #     mmPort = "8080"
    #     musicMaster = mmRequest.MMRequest("API Server", mmServer, mmPort)
    #     musicMaster.setStation("WMTU")
    #     setupBypass = 1
    #     return

    def setup(self, stationID):
        #global setupBypass, mmServer, mmPort, musicMaster
        if (self.setupBypass == 1):
            return
        self.mmServer = "http://10.10.50.18"
        self.mmPort = "8080"
        self.musicMaster = mmRequest.MMRequest("API Server", self.mmServer, self.mmPort)
        if (stationID != ""):
            self.musicMaster.setStation(stationID)
        else:
            self.musicMaster.setStation("WMTU")
        self.setupBypass = 1
        return

    def initInstance(self):
        global musicMaster
        #setup()
        return musicMaster

    def buildBody(self, root, data=[]):
        #print("Hello")
        #setup()
        for i in range(len(data)):
            self.musicMaster.addSubElement(root, element=data[i][0], text=data[i][1], attrib=data[i][2])
        return root

    #def setStation(instance, station):
    #        instance.setStation(station)

    #def setVersion(instance, version):
    #        instance.setVersion(version)

    #################################
    #### NEXUS XML COMMUNICATION ####
    #################################

    def ping(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("ping").text)

    def getServerInfo(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getServerInfo").text)

    def getStations(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getStations").text)

    def getStationInfo(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getStationInfo").text)

    def getFieldList(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getFieldList").text)

    def getAttributeList(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getAttributeList").text)

    def getCategoryList(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getCategoryList").text)

    def getSongLists(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getSongLists").text)

    def getAPIVersion(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getAPIVersion").text)

    def getCategoryGroups(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getCategoryGroups").text)

    def getFormatLists(self, stationID=""):
        # tested
        self.setup(stationID)
        return ET.fromstring(self.musicMaster.runCommand("getFormatLists").text)
    # implement getAuthToken and destroyAuthToken.  nexus api currently unauthenticated

    # build xml for get schedule command
    def getSchedule(self, data, stationID=""):
        # tested
        self.setup(stationID)
        #print(instance)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getSchedule")
        # write first layer values
        parent = self.buildBody(xmlRoot[0], data[0])
        # insert schedule properties
        self.buildBody(parent[2], data[1])
        # insert desired field return values
        self.buildBody(parent[3], data[2])
        #self.printXML(xmlRoot)
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getStationProperty(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getStationProperty")
        # write first layer properties
        parent = self.buildBody(xmlRoot[0], data[0])
        # insert station properties
        self.buildBody(parent[0], data[1])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def setStationProperty(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("setStationProperty")
        # write first layer properties
        parent = self.buildBody(xmlRoot[0], data[0])
        # insert station properties
        self.buildBody(parent[0], data[1])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getSongsByList(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getSongsByList")
        # write first layer properties
        parent = self.buildBody(xmlRoot[0], data[0])
        # insert field properties
        self.buildBody(parent[0], data[1])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getSongByQuery(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getSongsByQuery")
        # write first layer properties
        parent = self.buildBody(xmlRoot[0], data[0])
        # insert query parameters
        child = self.buildBody(parent[0], data[1])
        # insert categories
        self.buildBody(child[0], data[2])
        # insert filter properties
        self.buildBody(child[1], data[3])
        # insert sortKey properties
        self.buildBody(child[2], data[4])
        # insert extra properties
        self.buildBody(child[3], data[5])
        # insert field properties
        self.buildBody(parent[1], data[6])
        #for element in parent:
            #print(element.tag)
        #return ET.tostring(xmlRoot, encoding='utf8').decode('utf8')
        # convert to string and send request
        #self.printXML(xmlRoot)
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getSongInfo(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getSongInfo")
        # write first layer properties
        parent = self.buildBody(xmlRoot[0], data[0])
        # insert songList parameters
        self.buildBody(parent[0], data[1])
        # insert field properties
        self.buildBody(parent[1], data[2])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getHistory(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getHistory")
        # write first layer properties
        self.buildBody(xmlRoot[0], data[0])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getSongHistory(self, data, stationID=""):
        # tested
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getSongHistory")
        # write first layer properties
        parents = self.buildBody(xmlRoot[0], data[0])
        # write songlist properties
        self.buildBody(parents[2], data[1])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def getScheduleHistory(self, data, stationID=""):
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getScheduleHistory")
        # write first layer properties
        parents = self.buildBody(xmlRoot[0], data[0])
        # insert query properties
        child = self.buildBody(parents[2], data[1])
        # insert category properties
        self.buildBody(child[0], data[2])
        # insert filter properties
        self.buildBody(child[1], data[3])
        # insert sortKey properties
        self.buildBody(child[2], data[4])
        # insert additional properties
        self.buildBody(child[3], data[5])
        # convert to string and send request
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)

    def dontUSEgetSongsByQuery(self, data, stationID=""):
        self.setup(stationID)
        # build root
        xmlRoot = self.musicMaster.buildRoot("getSongByQuery")
        # write first layer properties
        parents = self.buildBody(xmlRoot[0], data[0])
        # insert query parameters
        child = self.buildBody(parents[0], data[1])
        # insert categories
        self.buildBody(child[0], data[2])
        # insert filters
        self.buildBody(child[1], data[3])
        # insert sortkeys
        self.buildBody(child[2], data[4])
        # insert properties
        self.buildBody(child[3], data[5])
        # insert fields
        child = self.buildBody(parents[1], data[6])
        return ET.fromstring(self.musicMaster.sendRequest(xmlRoot).text)


    # run musicmaster setup to initialize methods
    # must be run for module to work correctly
    #setup("")


    #root = musicMaster.buildRoot("getSchedule")
    #data = getSchedule(root, musicMaster, data)
    #getSongInfo(musicMaster, data)
    #dest = getSongsByQuery(musicMaster, data)
    #data = ET.tostring(dest, encoding='utf8').decode('utf8')
    #print(dest.text)

    #xml = xml.dom.minidom.parseString(dest)
    #xml_pretty_str = xml.toprettyxml()
    #print (xml_pretty_str)

    #getHistory(musicMaster, data)
    #print(getCategoryGroups().text)
    #getScheduleHistory(musicMaster, data)

    #### getScheduleHistory ####
    # contents = []
    # contents.append(["startTime", "2020-06-09T00:00:00", {}])
    # contents.append(["endTime", "2020-06-09T01:00:00", {}])
    # contents.append(["properties", "", {}])
    # contents.append(["fields", "", {}])
    #
    # properties = []
    # properties.append(["property", "True", {"name": "songList"}])
    #
    # fields = []
    # # fields.append(["field", "", {"id": "12"}])
    # fields.append(["field", "", {"name": "Category"}])
    # fields.append(["field", "", {"name": "WO Cat"}])
    # # fields.append(["field", "", {"name": "Artist"}])
    # # fields.append(["field", "", {"name": "Title"}])
    #
    # data3 = []
    # data3.append(contents)
    # data3.append(properties)
    # data3.append(fields)

    #getSchedule(data)

    #print(getScheduleHistory(initInstance(), data).text)


