from __future__ import print_function
from nexus import webAutomationSchedule as WAS
import datetime
import pickle
import os.path
from googleapiclient.discovery import build
from google.oauth2 import service_account
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


import sys
#from googleapiclient.discovery import build
#from oauth2client.service_account import ServiceAccountCredentials
SERVICE_ACCOUNT_FILE = ""
SCOPES = ""
SERVICE = ""

def getCalendarEvents(calendarID, dateMin=str(datetime.date.today()) + "T" + "0:00:00-05:00", dateMax=str((datetime.date.today() + datetime.timedelta(days=6))) + "T" + "23:59:59-05:00"):
    #dateMin = str(datetime.date.today()) + "T" + "0:00:00-05:00"
    #dateMax = str((datetime.date.today() + datetime.timedelta(days=6))) + "T" + "23:59:59-05:00"

    cEvents = []

    # Read events from the DJ Google Calendar Schedule
    page_token = None
    while True:
        global SERVICE
        events = SERVICE.events().list(calendarId=calendarID, timeMin=dateMin, timeMax=dateMax, pageToken=page_token).execute()
        for event in events['items']:
            cEvents.append(event['id'])
        page_token = events.get('nextPageToken')
        if not page_token:
            break

    # Get calendar details for each event
    for eventID in cEvents:
        event = SERVICE.events().get(calendarId=calendarID, eventId=eventID).execute()
        #print(event.get('summary', "DJ Scheduled Show"))
        #print(event.get('description',"DJ Live"))
        #print(event.get('updated',""))
        test = event.get('start',"")
        #print(test['dateTime'])
        print(test)
        print(event.get('end',""))

    # Prepare events for Music Calendar

def googleAuth():
    # Google Credentials
    global SERVICE_ACCOUNT_FILE, SCOPES, SERVICE
    SERVICE_ACCOUNT_FILE = 'auth/wmtu-music-scheduling-cb027d20146a.json'
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    # Authenticate
    credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES)
    SERVICE = build('calendar', 'v3', credentials=credentials)

def main():

    # Call the Calendar API
    #now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    #print('Getting the upcoming 10 events')
    #events_result = SERVICE.events().list(calendarId='c_chcdv056n1f5u6hm3jl3l2hsc4@group.calendar.google.com', timeMin=now,
    #                                    maxResults=10, singleEvents=True,
    #                                    orderBy='startTime').execute()
    #events = events_result.get('items', [])


    djCalendar = 'mtu.edu_st7ihirh2te644hrhj3d4vbric@group.calendar.google.com'
    musicCalendar = 'c_chcdv056n1f5u6hm3jl3l2hsc4@group.calendar.google.com'

    # authenticate to google
    googleAuth()
    # get Dj Calendar Events
    getCalendarEvents(djCalendar)
    # get automation schedule
    schedule = WAS.getWeek()
    #
    print(schedule)

def createEvent():
    if not events:
        print('No upcoming events found.')
    for event in events:
        start = event['start'].get('dateTime', event['start'].get('date'))
        print(start, event['summary'])

    event = {
        'summary': 'Google I/O 2015',
        'location': 'G03W Wadsworth Hall, 1703 Townsend Dr, Houghton, MI 49931',
        'description': 'A chance to hear more about Google\'s developer products.',
        'start': {
            'dateTime': '2020-08-17T09:00:00-07:00',
            'timeZone': 'America/Los_Angeles',
        },
        'end': {
            'dateTime': '2020-08-17T17:00:00-07:00',
            'timeZone': 'America/Los_Angeles',
        },
        # 'recurrence': [
        #    'RRULE:FREQ=DAILY;COUNT=2'
        # ],
        # 'attendees': [
        #    {'email': 'lpage@example.com'},
        #    {'email': 'sbrin@example.com'},
        # ],
        # 'reminders': {
        #    'useDefault': False,
        #    'overrides': [
        #        {'method': 'email', 'minutes': 24 * 60},
        #        {'method': 'popup', 'minutes': 10},
        #    ],
        # },
    }

    event = SERVICE.events().insert(calendarId='c_chcdv056n1f5u6hm3jl3l2hsc4@group.calendar.google.com',
                                    body=event).execute()
    print
    'Event created: %s' % (event.get('htmlLink'))
    print("Listing Events")

if __name__ == '__main__':
    main()

#    if __name__ == '__main__':
#        main(sys.argv)


# get a weeks worth of DJ events from the DJ Calendar
# gather automation schedule
# DJ Shows take precedence and overwrite automation time slots
# create google events for